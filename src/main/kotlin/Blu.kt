import java.util.*

fun String.upperCase(): String {
    return this.toUpperCase()
}

fun String.lowerCase(): String {
    return this.toLowerCase()
}